// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        //マップのぷれふぁぶ
        map1: {
            default: null,
            type: cc.Prefab,
        },
        map2: {
            default: null,
            type: cc.Prefab,
        },
        item: {
            default:null,
            type:cc.Node,
        },
        mapObject: {
            default:null,
            type:cc.Node,
        },
        player: {
            type:cc.Node,
            default:null,
        },
        keeper: {
            type:cc.Node,
            default:null,
        },
        //マップ番号
        number: null,
        //マップの詳細情報
        info: null,
        //マップの高さ
        height: null,
        //マップの幅
        width: null,
        //移動速度
        moveDuration: 0.5,
        //移動幅
        moveRange: 48,
        //プレイヤーの場所
        playerPositionX: null,
        playerPositionY: null,
        actionEnd: null,
        map: null,
        //追われているか
        beingChased: false,
    },


    // LIFE-CYCLE CALLBACKS:

    //読み込み時
    onLoad() {
        this.playerPositionX = 1;
        this.playerPositionY = 1;
        this.beingChased = 0;

        this.map1Init();
        //キー入力を受け取れるように
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onkeydown, this);


    },

    calcMapPos: function() {
        const x = (this.playerPositionX - 1) * -48;
        const y = (this.height - 1 - this.playerPositionY - 1) * -48;
        return cc.v2(x, y);
    },

    //キーが押されたとき
    onkeydown: function(event) {
        let currentX = this.playerPositionX;
        let currentY = this.playerPositionY;
        switch (event.keyCode) {
            case cc.macro.KEY.d:
                console.log("Press d");
                //移動先が存在する、移動可能か
                if (this.mapObject.getComponent("MapObject").info[currentX + currentY * this.width + 1] >= 150 || this.actionEnd !== null) {
                    console.log("stop d");
                    return;
                } else {
                    console.log("move d");
                    this.setLeftMove();
                    this.playerPositionX = ++currentX;
                }
                break;

            case cc.macro.KEY.a:
                console.log("Press a");
                //移動先が存在する、移動可能か
                if (this.mapObject.getComponent("MapObject").info[currentX + currentY * this.width - 1] >= 150 || this.actionEnd !== null) {
                    console.log("stop a");
                    return;
                } else {
                    console.log("move a");
                    this.setRightMove();
                    this.playerPositionX = --currentX;
                }
                break;

            case cc.macro.KEY.s:
                console.log("Press s");
                //移動先が存在する、移動可能か
                if (this.mapObject.getComponent("MapObject").info[currentX + (currentY + 1) * this.width] >= 150 || this.actionEnd !== null) {
                    console.log("stop s");
                    return;
                } else {
                    console.log("move s");
                    this.setUpMove();
                    this.playerPositionY = ++currentY;
                }
                break;

            case cc.macro.KEY.w:
                console.log("Press w");
                //移動先が存在する、移動可能か
                if (this.mapObject.getComponent("MapObject").info[currentX + (currentY - 1) * this.width] >= 150 || this.actionEnd !== null) {
                    console.log("stop w");
                    return;
                } else {
                    console.log("move w");
                    this.setDownMove();
                    this.playerPositionY = --currentY;
                }
                break;
            //デバッグ情報用
            case cc.macro.KEY.q:
                console.log("X:" + currentX);
                console.log("Y:" + currentY);
                console.log(this.info);
                break;
            //同時移動検査
            case cc.macro.KEY.r:
                const move = cc.moveBy(this.moveDuration, cc.v2(50,50));
                this.node.runAction(move);
                break;
            case cc.macro.KEY.enter:
                this.itemExist();
            default:
                console.log("関係ないキー");
                break;
        }


        //切り替え地点かチェック
        if (this.info[currentX + currentY * this.width] === 100) {
            this.mapChange();
        }
    },

    calcItemDistance:function(whichItem){
        const itemPos = whichItem.getPosition();
        const playerPos= this.player.getPosition();
        const distance = playerPos.sub(itemPos).mag();
        console.log(distance);
        return distance;
    },

    itemExist:function(){
        //playerから向いてる向きもらってあしもとか目の前のものに対してアクションを起こす。
        if (this.calcItemDistance(this.item.getComponent("Item").item1) < 48){
            this.item.getComponent("Item").getItem(1);
            return;
        }
        if (this.calcItemDistance(this.item.getComponent("Item").item2) < 48){
            this.item.getComponent("Item").getItem(2);
            return;
        }


        //イベントがあるかチェックする
    },

    //現在マップに応じてそれぞれのマップの切り替え関数へ
    mapChange: function() {
        console.log("mapChange before Map is " + this.number);
        switch (this.number) {
            case 1:
                this.map1NextMap();
                break;
            case 2:
                this.map2NextMap();
                break;
        }
    },

    //--------------------------
    //移動の方向を指定する関数群
    //--------------------------
    setUpMove: function() {
        this.move(cc.v2(0, this.moveRange));
    },
    setDownMove: function() {
        this.move(cc.v2(0, -this.moveRange));
    },
    setRightMove: function() {
        this.move(cc.v2(this.moveRange, 0));
    },
    setLeftMove: function() {
        this.move(cc.v2(-this.moveRange, 0));
    },

    //--------------
    //移動を実行
    //--------------
    move: function(vector) {
        //moveBy(移動にかかる時間、移動距離)
        //cc.v2(x,y)移動するベクトル
        const move = cc.moveBy(this.moveDuration, vector);

        //飼育員をマップに追従させる
        if (this.beingChased) {
            this.keeper.getComponent("Keeper").followMap(vector);
        }

        this.item.getComponent("Item").followMap(vector);
        this.actionEnd = this.map.runAction(move);
        this.scheduleOnce(function() {
            this.actionEnd = null;
        }, this.moveDuration);
    },

    //-------------------
    //飼育員が出現するか
    //-------------------
    willAKeeperSpawn: function() {
        this.beingChased = !this.beingChased && Math.round(Math.random() * 100) < 1000;
        console.log("beingchased:"+this.beingChased);
        if (this.beingChased){
            this.keeper.getComponent("Keeper").chaseStart();
        }
    },

    /*------------------------
    *map1関連の関数群
    *
    --------------------------*/
    //マップ１からほかのマップに移動するよ
    map1NextMap: function() {
        this.node.getChildByName("map").destroy();
        if (5 === this.playerPositionX && 1 === this.playerPositionY) {
            this.playerPositionX = 1;
            this.playerPositionY = 5;
            console.log("map2 ready");
            this.map2Init();
        }
        this.willAKeeperSpawn();
        if (this.beingChased) {
            console.log("spawn");
        }
    },

    map1Init: function() {
        //ぷれふぁぶを表示する部分
        this.map = cc.instantiate(this.map1);
        this.node.addChild(this.map, 1, "map");
        //マップの情報とかをセットしていくにょ
        this.number = 1;
        this.width = 7;
        this.height = 5;
        //infoいらないかもしれない
        this.info = [90, 90, 90, 90, 90, 90, 90,
            90, 10, 10, 10, 11, 99, 90,
            90, 11, 11, 10, 11, 11, 90,
            90, 99, 11, 10, 10, 10, 90,
            90, 90, 90, 90, 90, 90, 90
        ];
        //マップの座標、プレイヤーの場所が適切な位置になるように
        this.map.setPosition(this.calcMapPos());

        //オブジェクトの表示
        this.mapObject.getComponent("MapObject").map1load(this.calcMapPos());

        //アイテム表示
        this.item.getComponent("Item").map1load();
    },
    /*------------------------
    *map2関連の関数群
    *
    --------------------------*/

    map2NextMap: function() {
        this.node.getChildByName("map").destroy();
        if (3 === this.playerPositionX && 1 === this.playerPositionY) {
            this.playerPositionX = 1;
            this.playerPositionY = 3;
            console.log("map1 ready");
            this.map1Init();
        }
        if (1 === this.playerPositionX && 5 === this.playerPositionY) {
            this.playerPositionX = 5;
            this.playerPositionY = 1;
            this.map1Init();
        }

        this.willAKeeperSpawn();
        if (this.beingChased) {
            console.log("spawn");
        }
    },

    //初期化
    map2Init: function() {
        //ぷれふぁぶを表示する部分
        this.map = cc.instantiate(this.map2);
        this.node.addChild(this.map, 1, "map");
        //マップの情報とかをセットしていくにょ
        this.number = 2;
        this.width = 5;
        this.height = 7;
        this.info = [90, 90, 90, 90, 90,
            90, 10, 11, 99, 90,
            90, 10, 11, 11, 90,
            90, 10, 10, 10, 90,
            90, 11, 11, 10, 90,
            90, 99, 11, 10, 90,
            90, 90, 90, 90, 90
        ];
        //マップの座標、プレイヤーの場所が適切な位置になるように
        this.map.setPosition(this.calcMapPos());
    },


    //start () {},

    //update (dt){},
});
