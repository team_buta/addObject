// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        eventManager: {
            default: null,
            type: cc.Node,
        },
        map: {
            default: null,
            type: cc.Node,
        },
        itemList: {
            default: [],
        },
        image: {
            default: null,
            type: cc.Prefab,
        },
        item1: {
            default: null,
        },
        item1data:{
            default: null,
        },
        item2: {
            default: null,
        },
        item2data: {
            default: null,
        },
        map1: {
            default: null,
            type: cc.Prefab,
        },
        map2: {
            default: null,
            type: cc.Prefab,
        },
    },

    map1load: function() {
        //いままで表示していたアイテムを削除
        this.destroyBeforeItems();
        //そのアイテムを保有しているかのチェック。持っていないときのみ表示する。
        if (this.itemList[0] === true) {
            return;
        } else {
            //アイテムアイコン。出さなくてもＯＫ。
            this.item1 = cc.instantiate(this.image);
            this.node.addChild(this.item1, 2, "item1");
            //座標はそれぞれ指定
            this.item1.setPosition(this.itemPosition(3,2));
            //アイテムリストの何番目か
            this.item1data = 0;
            console.log("item set comp");
        }
    },

    //アイテムのマップ上の位置から画面上のどこに表示されるのか計算する
    itemPosition: function(x, y) {
        return cc.v2((x - this.map.getComponent("Map").playerPositionX) * 48, (this.map.getComponent("Map").playerPositionY - y) * 48);
    },

    //今まで表示していたアイテムを削除
    destroyBeforeItems:function(){
        if (this.item1 !== null){
            this.node.getChildByName("item1").destroy();
            this.item1 = null;
        }
        if (this.item2 !== null){
            this.node.getChildByName("item2").destroy();
            this.item2 = null;
        }
    },

    followMap:function(vector){
        if (this.item1 !== null){
            let move = cc.moveBy(0.5,vector);
            this.node.getChildByName("item1").runAction(move);
        }
        if (this.item2 !== null){
            let move = cc.moveBy(0.5,vector);
            this.node.getChildByName("item2").runAction(move);
        }
    },

    getItem:function(itemNum){
          if (itemNum === 1){
              this.itemList[this.item1data] = true;
              this.node.getChildByName("item1").destroy();
              this.item1 = null;
          }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        //アイテムを持っているかのリスト。現状１１個
        this.itemList = [false, false, false, false, false, false, false, false, false, false, false ];
    },

    // start () {},

    // update (dt) {},
});
