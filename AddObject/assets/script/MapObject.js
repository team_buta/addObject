// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        info:null,
        map1:{
            type:cc.Prefab,
            default:null,
        },
        lastMapName:null,
    },

    // LIFE-CYCLE CALLBACKS:

    //切り替えたいマップ名にする
    Map1load:function (mapPosition) {
        //マップの情報を記入。下記参照
        //https://docs.google.com/spreadsheets/d/1REJh75kOugo9iHewbrGIS-3wSviRaFWh8XkfwT9sqFM/edit#gid=414971237
        this.info=[
            0,0,0,0,0,0,0,
            0,0,0,0,0,0,0
        ];
        //前のマップを削除
        this.node.getChildByName(this.lastMapName).destroy();
        //マップ名の切り替え
        this.lastMapName="map1";
        //プレファブの用意
        let map1 = cc.instantiate(this.map1);
        //プレファブの表示
        this.node.addChild(map1,2,this.lastMapName);
        //マップの場所調整
        this.node.getChildByName(this.lastMapName).setPosition(mapPosition);
    },

    //マップの移動に追従
    followMap: function(vector) {
        let move = cc.moveBy(0.5, vector);
        this.node.getChildByName(this.lastMapName).runAction(move);
    },


    // onLoad () {},

    //start () {},

    // update (dt) {},
});
